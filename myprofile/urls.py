from django.urls import path
from .views import index, team
# url for app
urlpatterns = [
    path('', index, name='index'),
    path('friends/', team, name='team'),
]
