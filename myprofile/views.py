from django.shortcuts import render
# Create your views here.
def index(request):
    return render(request, 'profile.html')

def team(request):
    return render(request, 'friends.html')
