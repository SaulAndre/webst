from django.shortcuts import render
from .forms import Projects_Form
from .models import Project
# Create your views here.
def projects(request):
    if request.method == "POST":
        ini_form = Projects_Form(request.POST)
        if ini_form.is_valid():
            ini_form.save()
    else:
        ini_form = Projects_Form()
    dari_db = Project.objects.all()
    content = {'form':ini_form, 'data':dari_db}
    return render(request, 'projects.html', content)

    # if form.is_valid():
    #     judul = form.cleaned_data['judul']
    #     tgl_mulai = form.cleaned_data['tgl_mulai']
    #     tgl_selesai = form.cleaned_data['tgl_selesai']
    #     deskripsi = form.cleaned_data['deskripsi']
    #     tempat = form.cleaned_data['tempat']
    #     kategori = form.cleaned_data['kategori']

    #     # projects = Project(judul=judul, tgl_mulai=tgl_mulai, tgl_selesai=tgl_selesai,
    #     #                     deskripsi=deskripsi, tempat=tempat, kategori=kategori)
    #     form.save()
    
    # allProjects = Project.objects.all()
    # content = {'forms':form, 'project':allProjects}
    
    # return render(request, "projects.html", content)