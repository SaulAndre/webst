from django.db import models

# Create your models here.
class Project(models.Model):
    judul = models.CharField(max_length = 20)
    tgl_mulai = models.DateField()
    tgl_selesai = models.DateField()
    deskripsi = models.CharField(max_length=30)
    tempat = models.CharField(max_length=50)
    kategori = models.CharField(max_length=30)

    def __str__(self):
        return self.judul