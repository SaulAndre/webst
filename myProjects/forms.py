from django import forms
from .models import Project


class Projects_Form(forms.ModelForm):
    judul = forms.CharField(label='', widget=forms.TextInput(
        attrs={'placeholder': 'Judul', 'class': 'form-control'}))
    tgl_mulai = forms.DateField(label='', widget=forms.TextInput(
        attrs={'placeholder': 'Tanggal Mulai', 'class': 'form-control'}))
    tgl_selesai = forms.DateField(label='', widget=forms.TextInput(
        attrs={'placeholder': 'Tanggal Selesai', 'class': 'form-control'}))
    deskripsi = forms.CharField(label='', widget=forms.TextInput(
        attrs={'placeholder': 'Deskripsi', 'class': 'form-control'}))
    tempat = forms.CharField(label='', widget=forms.TextInput(
        attrs={'placeholder': 'Tempat', 'class': 'form-control'}))
    kategori = forms.CharField(label='', widget=forms.TextInput(
        attrs={'placeholder': 'Kategori', 'class': 'form-control'}))

    class Meta:
        model = Project
        fields = ['judul', 'tgl_mulai', 'tgl_selesai',
                  'deskripsi', 'tempat', 'kategori']
        # widgets = {
        #     'tgl_mulai': forms.DateInput(attrs={'type': 'date'}),
        #     'tgl_selesai': forms.DateInput(attrs={'type': 'date'}),
        # }
